var Analysis = (function () {

    var _biAnalysisEndpoint = '/bi/analysis';
    var _biMedicationsEndpoint = '/bi/medications';
    var _biPatientsByTypeEndpoint = '/bi/patients/:type';

    function Analysis() {
        // No-op
    }

    Analysis.prototype = {
        medications: function(success, failure) {
            $.getJSON(_biMedicationsEndpoint,
                function callback(result, status, xhr) {
                    if ("success" === status) {
                        success(result);
                    } else {
                        failure(result);
                    }
                }
            );
        },
        getPatientsByType: function(tpe, success, failure) {
            $.getJSON(_biPatientsByTypeEndpoint.replace(":type", tpe),
                function callback(result, status, xhr) {
                    if ("success" === status) {
                        success(result);
                    } else {
                        failure(result);
                    }
                }
            );
        },
        getBIAnalysis: function (success, failure) {
            $.getJSON(_biAnalysisEndpoint,
                function callback(result, status, xhr) {
                    if ("success" === status) {
                        success(result);
                    } else {
                        failure(result);
                    }
                }
            );
        }
    };

    return new Analysis();
})();


