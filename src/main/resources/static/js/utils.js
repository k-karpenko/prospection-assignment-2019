Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
};

function groupBy(xs, key) {
    return xs.reduce(function(rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
};

function shallowCompare(newObj, prevObj){
    if ((newObj == null || prevObj == null) && (newObj !== prevObj)) return false;

    for (var key in newObj){
        if(newObj[key] !== prevObj[key]) return false;
    }
    return true;
}