window.Application = (() => {
    const PatientType = {
        VALID_NO_COMED: "VALID_NO_COMED",
        VIOLATED: "VIOLATED"
    };

    let $state = {
        charts: {
            timeline: null,
            patientsList: null,
            piechart: null
        },
        chartOptions: {
            typeShiftsTableOptions: {

            },
            infoTableOptions: {
                width: '100%',
                height: '100%'
            },
            piechartOptions:  {
                title: 'B & I Analysis Result',
                pieHole: 0.4
            },
            patientsList: {
                hideEmptyRecords: true
            },
            timelineOptions: {
                width: 1200,
                height: 185,
                mergeNeighbouring: true,
                avoidOverlappingGridLines: true
            }
        },
        chartsData: {
            raw: {
                patientsByType: null
            },
            dataFrames: {
                patientsList: null
            }
        },
        beginingOfTime: new Date(),
        medications: {},
        colorMap: {
            types: {
                VIOLATION: 'red',
                FirstShift: 'yellow'
            },
            medicines: {
                I: 'black',
                B: 'blue'
            }
        }
    };

    const $dataTypes = {
        timelineData: {
            createDataFrame: () => {
                const data = new google.visualization.DataTable();
                data.addColumn({type: 'string', id: 'Patient ID'});
                data.addColumn({type: 'string', id: 'Medication'});
                data.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
                data.addColumn({type: 'string', role: 'style'});
                data.addColumn({type: 'date', id: 'Start'});
                data.addColumn({type: 'date', id: 'End'});

                return data;
            },
            createRow: (patientId, medication, isFirstShift, isViolation, style, start, end) => {
                return [
                    "Patient " + patientId.toString(),
                    medication,
                    (isViolation ? "[violation] " : "") + "Started at day <strong>" + start + "</strong> for a duration of " +
                        $state.medications[medication].toString() + " days" + (isFirstShift && !isViolation ? " <u>(type changed)</u>" : ""),
                    style,
                    $state.beginingOfTime.addDays(start),
                    $state.beginingOfTime.addDays(end)
                ]
            }
        },
        patientsList: {
            createRow: (patientId, purchasesList) => {
                return [
                    {v: patientId},
                    purchasesList
                ]
            },
            createDataFrame: () => {
                const data = new google.visualization.DataTable();
                data.addColumn('number', 'Patient ID');
                data.addColumn('string', 'Purchases');

                return data;
            }
        },
        typeShiftsData: {
            createDataFrame: () => {
                const data = new google.visualization.DataTable();
                data.addColumn('string', 'Task ID');
                data.addColumn('string', 'Task Name');
                data.addColumn('date', 'Start Date');
                data.addColumn('date', 'End Date');
                data.addColumn('number', 'Duration');
                data.addColumn('number', 'Percent Complete');
                data.addColumn('string', 'Dependencies');
                return data;
            },
            createRow: (idx, fromState, toState) => {
                return [
                    toState,
                    toState,
                    $state.beginingOfTime.addDays(idx),
                    $state.beginingOfTime.addDays(idx + 1),
                    1,
                    100,
                    null
                ];
            }
        }
    };

    const refreshPatientsList = () => {
        var patientsData = $dataTypes.patientsList.createDataFrame();

        $state.chartsData.dataFrames.patientsList = patientsData;

        $state.chartsData.raw.patientsByType.forEach((patientRecord) => {
            if ($state.chartOptions.patientsList.hideEmptyRecords && patientRecord.purchases.length === 0) {
                return;
            }

            const firstShift = patientRecord.shiftsHistory.length ? patientRecord.shiftsHistory[0] : null;
            const hasViolation = patientRecord.shiftsHistory.filter(v => v.currentType === PatientType.VIOLATED);
            let purchasesList = "No Purchases Recorded";
            if  (patientRecord.purchases.length !== 0) {
                purchasesList = "<ol>" +
                    patientRecord.purchases.map((purchase) => {
                        var isFirstShift = firstShift && shallowCompare(firstShift.record, purchase);
                        return "<li" + (isFirstShift && !hasViolation ? " class='important-red'" : "") + ">" + purchase.medication + " (day " + purchase.day + (isFirstShift ? " <b>[type changed]</b>" : "") + ")</li>";
                    }).join("")
                    + "</ol>";
            }

            patientsData.addRow($dataTypes.patientsList.createRow(patientRecord.patientId, purchasesList));
        });

        document.getElementById("statistics").classList.remove("hidden");
        $this.drawPatientsTable(patientsData);
    };

    const refreshTimeline = (table) => {
        if (table.getSelection().length === 0) return;
        if (!$state.chartsData.raw.patientsByType) return;

        const selectedRecordIdx = table.getSelection()[0].row;
        const selectedRecordPatientId = $state.chartsData.dataFrames.patientsList.getValue(selectedRecordIdx, 0);
        const patient = $state.chartsData.raw.patientsByType.filter(v => v.patientId === selectedRecordPatientId)[0];
        if (!patient) return;

        const patientsTimeline = $dataTypes.timelineData.createDataFrame();
        const patientTypeShifts = $dataTypes.typeShiftsData.createDataFrame();

        mergeNeighbouring(patient, patient.purchases).forEach((purchase) => {
            patientsTimeline.addRow(
                $dataTypes.timelineData.createRow(
                    purchase.patientId,
                    purchase.medication,
                    purchase.isFirstShift,
                    purchase.isViolation,
                    purchase.isViolation ? $state.colorMap.types.VIOLATION :
                        (purchase.isFirstShift ? $state.colorMap.types.FirstShift : $state.colorMap.medicines[purchase.medication]),
                    purchase.day,
                    purchase.end
                )
            );
        });

        patient.shiftsHistory.forEach((item, i) => {
            if (item.currentType === item.previousType) return;

            patientTypeShifts.addRow(
                $dataTypes.typeShiftsData.createRow(
                    i,
                    item.previousType,
                    item.currentType
                )
            )
        });

        if (patient.shiftsHistory.length === 0) {
            patientTypeShifts.addRow(
                $dataTypes.typeShiftsData.createRow(
                    1,
                    null,
                    patient.currentType ? patient.currentType.currentType : PatientType.VALID_NO_COMED
                )
            )
        }

        $this.drawPurchasesTimeline(patientsTimeline, {
            tooltip: { isHtml: true }
        });

        $this.drawTypeShiftsTable(patientTypeShifts, {
            height: 400
        });
    };



    const onPatientTypeSelected = (chart, data, patientTypeNameMap) => {
        const selectedItem = chart.getSelection()[0];
        if (!selectedItem) return;

        const value = data.getValue(selectedItem.row || 0, selectedItem.column || 0);
        const typeNames = Object.keys(patientTypeNameMap).filter(v => patientTypeNameMap[v] === value);
        if (typeNames.length === 0) {
            console.error("Unknown patient type name", value);
            return;
        }

        const typeName = typeNames[0];

        Analysis.getPatientsByType(typeName, (result) => {
            $state.chartsData.raw.patientsByType = result;

            document.getElementById("subset_headline").textContent = value;

            refreshPatientsList();
        }, (error) => {
            toastr.error('Oops, failed to load patients list result.');
            console.error("Unable to load patients list", error);
        })
    }

    const mergeNeighbouring = (patient, purchases) => {
        let reshaped = [];
        const groupedPurchases = groupBy(purchases, 'medication');
        Object.keys(groupedPurchases).forEach((key) => {
            const group = groupedPurchases[key];

            let updated = [];
            const hasViolation = patient.shiftsHistory.filter(v => v.currentType === PatientType.VIOLATED).length > 0;
            group.forEach((item) => {
                const relatedShift = patient.shiftsHistory.filter(v => shallowCompare(v.record, item))[0];
                item.isViolation = relatedShift && relatedShift.currentType === PatientType.VIOLATED;
                item.isFirstShift = !hasViolation &&
                    (patient.shiftsHistory.length !== 0 && shallowCompare(patient.shiftsHistory[0], relatedShift));
                item.end = item.day + $state.medications[item.medication];

                if (updated.length === 0 || !$state.chartOptions.timelineOptions.mergeNeighbouring) {
                    updated.push(item);
                    return;
                }

                if (item.day >= updated[updated.length - 1].day && updated[updated.length - 1].end >= item.day) {
                    updated[updated.length - 1].end += Math.abs(item.end - updated[updated.length - 1].end)
                    updated[updated.length - 1].isViolation = item.isViolation || updated[updated.length - 1].isViolation;
                    updated[updated.length - 1].isFirstShift = !hasViolation
                        && (item.isFirstShift || updated[updated.length - 1].isFirstShift);
                } else {
                    updated.push(item);
                }
            });

            reshaped = reshaped.concat(updated);
        });

        return reshaped;
    }

    const $this = {
        init: (state) => {
            $state = Object.assign($state, state || {});
        },
        drawPatientsTable: (data) => {
            const table = new google.visualization.Table(document.getElementById('patients_div'));
            $state.charts.patientsList = table;
            google.visualization.events.addListener(table, 'select', refreshTimeline.bind(null, table));
            google.visualization.events.addListener(table, 'ready',  () => {
                table.setSelection([{row: 0, column: 0}]);
                refreshTimeline(table, data);
            });
            table.draw(data, {width: '100%', height: '100%', allowHtml: true});
        },
        drawInfoTable: (data) => {
            const table = new google.visualization.Table(document.getElementById('table_div'));
            table.draw(data, $state.chartOptions.infoTableOptions);
        },
        drawTypeShiftsTable: (data) => {
            const table = new google.visualization.Gantt(document.getElementById('type_shifts_div'));
            table.draw(data, $state.chartOptions.typeShiftsTableOptions);
        },
        drawChart: (data, patientTypeNameMap) => {
            const chart = new google.visualization.PieChart(document.getElementById('piechart'));
            google.visualization.events.addListener(chart, 'select', onPatientTypeSelected.bind(null, chart, data, patientTypeNameMap));
            $state.charts.piechart = chart;
            chart.draw(data, $state.chartOptions.piechartOptions);
        },
        drawPurchasesTimeline: (data, options) => {
            const container = document.getElementById('timeline_div');

            const table = new google.visualization.Timeline(container);
            $state.charts.timeline = table;
            table.draw(data, Object.assign({}, options || {}, $state.chartOptions.timelineOptions));
            document.getElementById("timeline_wrapper").setAttribute("style", "");
        },
        onHideEmptyRecordsChanged: (newState) => {
            $state.chartOptions.patientsList.hideEmptyRecords = newState;
            refreshPatientsList();
        },
        onMergeNeighboursChanged: (newState) => {
            $state.chartOptions.timelineOptions.mergeNeighbouring = newState;
            refreshTimeline($state.charts.patientsList);
        }
    };

    return $this;
})();