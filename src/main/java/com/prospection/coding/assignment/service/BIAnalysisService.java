package com.prospection.coding.assignment.service;

import static com.prospection.coding.assignment.domain.AnalysisResult.PatientType.*;
import static com.prospection.coding.assignment.utils.StreamUtils.*;

import com.prospection.coding.assignment.data.PurchaseRecordDAO;
import com.prospection.coding.assignment.domain.AnalysisResult;
import com.prospection.coding.assignment.domain.PatientData;
import com.prospection.coding.assignment.domain.PurchaseRecord;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BIAnalysisService {
    private static final Logger log = LoggerFactory.getLogger(BIAnalysisService.class);

    private final PurchaseRecordDAO purchaseRecordDAO;

    public static class PatientDataTypePredicate implements Predicate<PatientData> {
        private final AnalysisResult.PatientType type;

        public PatientDataTypePredicate(AnalysisResult.PatientType type) {
            this.type = type;
        }

        @Override
        public boolean test(PatientData patientData) {
            return patientData.finalType().name().equals(this.type.name());
        }
    }

    @Autowired
    public BIAnalysisService(PurchaseRecordDAO purchaseRecordDAO) {
        this.purchaseRecordDAO = purchaseRecordDAO;
    }

    public Stream<PatientData> processPatientData(Predicate<PatientData> predicate)
            throws Exception {
        List<PurchaseRecord> purchaseRecords = purchaseRecordDAO.allPurchaseRecords();

        ConcurrentHashMap<Integer, PatientData> patientData = new ConcurrentHashMap<>();

        purchaseRecords
                .stream()
                .collect(
                        Collectors.groupingBy(
                                PurchaseRecord::getPatientId,
                                toSortedList(Comparator.comparing(PurchaseRecord::getDay))))
                .forEach(
                        (key, value) -> {
                            PatientData record =
                                    patientData.computeIfAbsent(key, v -> new PatientData(key));
                            value.forEach(record::withPurchase);
                        });

        return patientData
                .values()
                .stream()
                .peek(PatientData::finish)
                .filter(predicate)
                .sorted(Comparator.comparing(PatientData::getPatientId));
    }

    public AnalysisResult performBIAnalysis() throws Exception {
        final Map<AnalysisResult.PatientType, Long> totalCounts =
                processPatientData((v) -> true)
                        .collect(
                                Collectors.groupingBy(
                                        PatientData::finalType, Collectors.counting()));

        final Function<AnalysisResult.PatientType, Integer> total =
                (type) -> Optional.ofNullable(totalCounts.get(type)).map(Long::intValue).orElse(0);

        AnalysisResult result = new AnalysisResult();
        result.putTotal(VIOLATED, total.apply(VIOLATED));
        result.putTotal(VALID_NO_COMED, total.apply(VALID_NO_COMED));
        result.putTotal(VALID_BI_SWITCH, total.apply(VALID_BI_SWITCH));
        result.putTotal(VALID_IB_SWITCH, total.apply(VALID_IB_SWITCH));
        result.putTotal(VALID_I_TRIAL, total.apply(VALID_I_TRIAL));
        result.putTotal(VALID_B_TRIAL, total.apply(VALID_B_TRIAL));
        return result;
    }
}
