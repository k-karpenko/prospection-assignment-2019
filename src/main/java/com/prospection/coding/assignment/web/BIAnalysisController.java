package com.prospection.coding.assignment.web;

import com.prospection.coding.assignment.domain.AnalysisResult;
import com.prospection.coding.assignment.domain.MedicationType;
import com.prospection.coding.assignment.domain.PatientData;
import com.prospection.coding.assignment.service.BIAnalysisService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("bi")
public class BIAnalysisController {

    private final BIAnalysisService biAnalysisService;

    @Autowired
    public BIAnalysisController(BIAnalysisService biAnalysisService) {
        this.biAnalysisService = biAnalysisService;
    }

    @RequestMapping("/medications")
    @ResponseBody
    public Map<String, Integer> medications() throws Exception {
        MedicationType[] values = MedicationType.values();
        Map<String, Integer> response = new HashMap<>();
        for (MedicationType type : values) {
            response.put(type.name(), type.duration());
        }
        return response;
    }

    @GetMapping("/patients/{type}")
    @ResponseBody
    public List<PatientData> patients(@PathVariable("type") String type) throws Exception {
        return biAnalysisService
                .processPatientData(
                        new BIAnalysisService.PatientDataTypePredicate(
                                AnalysisResult.PatientType.valueOf(type)))
                .collect(Collectors.toList());
    }

    @RequestMapping("/analysis")
    @ResponseBody
    public AnalysisResult analysis() throws Exception {
        return biAnalysisService.performBIAnalysis();
    }
}
