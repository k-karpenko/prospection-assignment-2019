package com.prospection.coding.assignment.utils;

import static java.util.concurrent.ConcurrentHashMap.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public final class StreamUtils {

    public static <T> Collector<T, ?, List<T>> toSortedList(Comparator<? super T> c) {
        return Collectors.collectingAndThen(
                Collectors.toCollection(ArrayList::new),
                l -> {
                    l.sort(c);
                    return l;
                });
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }
}
