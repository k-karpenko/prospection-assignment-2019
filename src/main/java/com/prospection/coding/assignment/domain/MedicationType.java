package com.prospection.coding.assignment.domain;

import static com.prospection.coding.assignment.domain.AnalysisResult.PatientType.*;

public enum MedicationType {
    I(90, VALID_I_TRIAL, VALID_BI_SWITCH),
    B(30, VALID_B_TRIAL, VALID_IB_SWITCH);

    private final int duration;
    private final AnalysisResult.PatientType trialType;
    private final AnalysisResult.PatientType switchType;

    MedicationType(
            int duration,
            AnalysisResult.PatientType trialType,
            AnalysisResult.PatientType switchType) {
        this.duration = duration;
        this.trialType = trialType;
        this.switchType = switchType;
    }

    public int duration() {
        return duration;
    }

    public AnalysisResult.PatientType trialType() {
        return trialType;
    }

    public AnalysisResult.PatientType switchType() {
        return switchType;
    }
}
