package com.prospection.coding.assignment.domain;

import java.util.Objects;

public class PurchaseRecord {

    private int day;
    private String medication;
    private int patientId;

    // required for JSON pickler
    private PurchaseRecord() {}

    public PurchaseRecord(int day, String medication, int patientId) {
        this.day = day;
        this.medication = medication;
        this.patientId = patientId;
    }

    public int getDay() {
        return day;
    }

    public PurchaseRecord setDay(int day) {
        this.day = day;
        return this;
    }

    public String getMedication() {
        return medication;
    }

    public PurchaseRecord setMedication(String medication) {
        this.medication = medication;
        return this;
    }

    public int getPatientId() {
        return patientId;
    }

    public PurchaseRecord setPatientId(int patientId) {
        this.patientId = patientId;
        return this;
    }

    public String toString() {
        return day + "," + medication + "," + patientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseRecord that = (PurchaseRecord) o;
        return day == that.day
                && patientId == that.patientId
                && Objects.equals(medication, that.medication);
    }

    @Override
    public int hashCode() {
        return Objects.hash(day, medication, patientId);
    }
}
