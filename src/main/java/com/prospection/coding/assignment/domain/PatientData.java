package com.prospection.coding.assignment.domain;

import static com.prospection.coding.assignment.domain.AnalysisResult.PatientType.VALID_NO_COMED;
import static com.prospection.coding.assignment.domain.AnalysisResult.PatientType.VIOLATED;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class PatientData {
    private static final Logger log = LoggerFactory.getLogger(PatientData.class);

    private Integer patientId;
    private final List<PurchaseRecord> purchases = new ArrayList<>();
    private final List<TypeShift> shiftsHistory = new ArrayList<>();

    private PurchaseRecord currentPurchase;
    private TypeShift currentType;

    private boolean isSealed;

    // required for JSON pickler
    private PatientData() {}

    public PatientData(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public int totalShifts() {
        return this.shiftsHistory.size();
    }

    public TypeShift getCurrentType() {
        return currentType;
    }

    public AnalysisResult.PatientType finalType() {
        if ((this.currentType != null && this.currentType.getCurrentType() == VIOLATED)
                || this.shiftsHistory
                        .stream()
                        .map((v) -> v.currentType)
                        .anyMatch((v) -> v == VIOLATED)) {
            return VIOLATED;
        }

        Optional<TypeShift> result = Optional.ofNullable(this.currentType);
        if (!this.shiftsHistory.isEmpty()) {
            result = this.shiftsHistory.stream().findFirst();
        }

        return result.map((v) -> v.currentType).orElse(VALID_NO_COMED);
    }

    public List<TypeShift> getShiftsHistory() {
        return shiftsHistory;
    }

    public PurchaseRecord getCurrentPurchase() {
        return currentPurchase;
    }

    public List<PurchaseRecord> getPurchases() {
        return purchases;
    }

    public void finish() {
        if (this.currentType != null) {
            this.purchases.add(this.currentPurchase);
        }

        if (this.currentType != null) {
            this.shiftsHistory.add(this.currentType);
        }

        this.isSealed = true;
    }

    protected void withTypeShift(PurchaseRecord record, AnalysisResult.PatientType targetType) {
        AnalysisResult.PatientType previousType = null;
        if (this.currentType != null) {
            this.shiftsHistory.add(this.currentType);
            previousType = this.currentType.getCurrentType();
        }

        MedicationType currentMedication =
                MedicationType.valueOf(this.currentPurchase.getMedication());

        TypeShift shift = new TypeShift(record, previousType, targetType);
        if (targetType != VIOLATED) {
            if (this.currentType != null && !this.shiftsHistory.isEmpty()) {
                int duration = currentMedication.duration();
                if (shift.record.getDay() - (this.currentType.record.getDay() + duration) < 1) {
                    log.debug(
                            "Patient "
                                    + this.patientId
                                    + " didn't come clean from the last trial/switch ("
                                    + currentMedication.name()
                                    + ") on "
                                    + this.currentType.record.getDay()
                                    + ", "
                                    + "so current switch/trial to "
                                    + shift.record.getMedication()
                                    + " is considered a violation on day "
                                    + shift.record.getDay());
                    shift = shift.withCurrentType(VIOLATED);
                }
            }
        }

        this.currentType = shift;
    }

    protected TypeShift findLastType(Predicate<TypeShift> predicate) {
        if (this.currentType != null && predicate.test(this.currentType)) return this.currentType;

        return this.shiftsHistory.stream().filter(predicate).reduce((l, r) -> r).orElse(null);
    }

    public void withPurchase(PurchaseRecord record) {
        if (this.isSealed) throw new IllegalStateException("unable to update sealed record");

        if (currentPurchase == null) {
            this.currentPurchase = record;
            return;
        }

        MedicationType medicationType = MedicationType.valueOf(record.getMedication());

        if (this.currentType != null && this.currentType.isConfirmationRequired()) {
            AnalysisResult.PatientType confirmedType =
                    this.currentType.confirmShift(medicationType);
            if (this.currentType.currentType != confirmedType) {
                log.debug(
                        "Type has been re-confirmed to be "
                                + confirmedType
                                + " (from "
                                + this.currentType.currentType
                                + ") for patient "
                                + this.patientId);
                this.currentType = this.currentType.withCurrentType(confirmedType);
            }
        }

        boolean isConflicting =
                (MedicationType.valueOf(this.currentPurchase.getMedication()).duration()
                                + this.currentPurchase.getDay())
                        > record.getDay();
        if (!record.getMedication().equals(this.currentPurchase.getMedication())) {
            if (isConflicting) {
                this.withTypeShift(record, medicationType.trialType());
            } else {
                this.withTypeShift(record, medicationType.switchType());
            }
        }

        this.purchases.add(this.currentPurchase);
        this.currentPurchase = record;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientData that = (PatientData) o;
        return Objects.equals(patientId, that.patientId)
                && Objects.equals(purchases, that.purchases)
                && Objects.equals(shiftsHistory, that.shiftsHistory)
                && Objects.equals(currentPurchase, that.currentPurchase)
                && Objects.equals(currentType, that.currentType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(patientId, purchases, shiftsHistory, currentPurchase, currentType);
    }

    public static final class TypeShift {
        private PurchaseRecord record;
        private AnalysisResult.PatientType previousType;
        private AnalysisResult.PatientType currentType;

        // required for JSON pickler
        private TypeShift() {}

        public TypeShift(
                PurchaseRecord record,
                AnalysisResult.PatientType previousType,
                AnalysisResult.PatientType currentType) {
            this.record = record;
            this.previousType = previousType;
            this.currentType = currentType;
        }

        public TypeShift withCurrentType(AnalysisResult.PatientType type) {
            return new TypeShift(this.record, this.previousType, type);
        }

        @Transient
        public boolean isConfirmationRequired() {
            return this.currentType == AnalysisResult.PatientType.VALID_B_TRIAL
                    || this.currentType == AnalysisResult.PatientType.VALID_I_TRIAL;
        }

        public AnalysisResult.PatientType confirmShift(MedicationType nextMedication) {
            AnalysisResult.PatientType confirmedType = this.currentType;
            switch (this.currentType) {
                case VALID_B_TRIAL:
                    if (nextMedication == MedicationType.B) {
                        confirmedType = AnalysisResult.PatientType.VALID_IB_SWITCH;
                    }
                    break;
                case VALID_I_TRIAL:
                    if (nextMedication == MedicationType.I) {
                        confirmedType = AnalysisResult.PatientType.VALID_BI_SWITCH;
                    }
                    break;
            }

            return confirmedType;
        }

        public PurchaseRecord getRecord() {
            return record;
        }

        public AnalysisResult.PatientType getPreviousType() {
            return previousType;
        }

        public AnalysisResult.PatientType getCurrentType() {
            return currentType;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TypeShift typeShift = (TypeShift) o;
            return Objects.equals(record, typeShift.record)
                    && previousType == typeShift.previousType
                    && currentType == typeShift.currentType;
        }

        @Override
        public int hashCode() {
            return Objects.hash(record, previousType, currentType);
        }
    }
}
