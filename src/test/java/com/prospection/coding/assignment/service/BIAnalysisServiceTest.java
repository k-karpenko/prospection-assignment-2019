package com.prospection.coding.assignment.service;

import static com.prospection.coding.assignment.domain.AnalysisResult.PatientType.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;

import com.prospection.coding.assignment.data.PurchaseRecordDAO;
import com.prospection.coding.assignment.domain.*;
import com.prospection.coding.assignment.domain.MedicationType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BIAnalysisServiceTest {

    @Mock private PurchaseRecordDAO purchaseRecordDAO;

    private BIAnalysisService biAnalysisService;

    @Before
    public void setUp() {
        biAnalysisService = new BIAnalysisService(purchaseRecordDAO);
    }

    @Test
    public void shouldOnlyReportAFirstTypeSwitch() throws Exception {
        final int SWITCH_AND_TRIAL_PATIENT_ID = 1;
        final int TRIAL_AND_SWITCH_PATIENT_ID = 2;
        final int VIOLATED_PATIENT_ID = 3;

        setupPatients(
                recordsBuilder()
                        // expected to be a violation (patient joined a second trial before he came
                        // clean)
                        .withPatient(
                                VIOLATED_PATIENT_ID,
                                (builder) ->
                                        builder.withPurchase(MedicationType.B)
                                                .withPurchase(MedicationType.B)
                                                .withTrial(MedicationType.I)
                                                .withPurchase(MedicationType.B)
                                                .withTrial(MedicationType.I)
                                                .withPurchases(MedicationType.B, 3)
                                                .withPurchases(MedicationType.I, 4))
                        // expected to be a switch (patient first switch and later on joined a
                        // trial)
                        .withPatient(
                                SWITCH_AND_TRIAL_PATIENT_ID,
                                (builder) ->
                                        builder.withPurchase(MedicationType.I)
                                                .withPurchase(MedicationType.B)
                                                .withPurchase(MedicationType.I)
                                                .withPurchase(MedicationType.I)
                                                .withTrial(MedicationType.B))
                        // expected to be a trial (patient joined a trial and later on switched to
                        // I)
                        .withPatient(
                                TRIAL_AND_SWITCH_PATIENT_ID,
                                (builder) ->
                                        builder.withPurchase(MedicationType.B)
                                                .withTrial(MedicationType.I)
                                                .withPurchase(MedicationType.B)
                                                .withPurchase(MedicationType.I))
                        .compile());

        AnalysisResult result = biAnalysisService.performBIAnalysis();

        assertThat("Number of valid I trials should be 1.", result.getTotal(VALID_I_TRIAL), is(1));
        assertThat("Number of violations should be 1.", result.getTotal(VIOLATED), is(1));
        assertThat(
                "Number of valid B switches should be 1.", result.getTotal(VALID_IB_SWITCH), is(1));
    }

    @Test
    public void shouldCorrectlyReportViolations() throws Exception {
        final int PATIENT_WITH_A_SECOND_TRIAL_IN_A_STREAK = 1;
        final int PATIENT_SWITCHED_WITHOUT_COMING_CLEAN = 2;

        setupPatients(
                recordsBuilder()
                        // expected to be a violator (a patient has joined a new trial before coming
                        // clean from the other trial they had)
                        .withPatient(
                                PATIENT_WITH_A_SECOND_TRIAL_IN_A_STREAK,
                                (builder) ->
                                        builder.withPurchases(MedicationType.I, 2)
                                                .withTrial(MedicationType.B)
                                                .withTrial(MedicationType.I))
                        .withPatient(
                                PATIENT_SWITCHED_WITHOUT_COMING_CLEAN,
                                (builder) ->
                                        builder.withPurchase(MedicationType.B)
                                                .withPurchase(MedicationType.B)
                                                .withPurchase(MedicationType.B)
                                                .withTrial(MedicationType.I)
                                                .withPurchase(MedicationType.B)
                                                .withTrial(MedicationType.I)
                                                .withPurchase(MedicationType.B))
                        .compile());

        AnalysisResult result = biAnalysisService.performBIAnalysis();

        assertThat("Number of valid I trials should be 2.", result.getTotal(VIOLATED), is(2));
    }

    @Test
    public void shouldCorrectlyReportTrials() throws Exception {
        final int FIRST_PATIENT_TRIALED_I_ID = 1;
        final int PATIENT_TRIALED_B_ID = 2;
        final int SECOND_PATIENT_TRIALED_I_ID = 3;

        setupPatients(
                recordsBuilder()
                        .withPatient(
                                PATIENT_TRIALED_B_ID,
                                (builder) ->
                                        builder.withPurchases(MedicationType.I, 3)
                                                .withTrial(MedicationType.B)
                                                .withPurchases(MedicationType.I, 4))
                        .withPatient(
                                FIRST_PATIENT_TRIALED_I_ID,
                                (builder) ->
                                        builder.withPurchases(MedicationType.B, 2)
                                                .withTrial(MedicationType.I))
                        .withPatient(
                                SECOND_PATIENT_TRIALED_I_ID,
                                (builder) ->
                                        builder.withPurchases(MedicationType.B, 2)
                                                .withTrial(MedicationType.I))
                        .compile());

        AnalysisResult result = biAnalysisService.performBIAnalysis();

        assertThat("Number of valid I trials should be 2.", result.getTotal(VALID_I_TRIAL), is(2));
        assertThat("Number of valid B trials should be 1.", result.getTotal(VALID_B_TRIAL), is(1));
    }

    @Test
    public void shouldCorrectlyReportTypeShifts() throws Exception {
        final int PATIENT_WITH_A_SINGLE_SWITCH_ID = 1;
        final int PATIENT_WITH_A_MULTIPLE_SWITCHES_ID = 2;

        setupPatients(
                recordsBuilder()
                        .withPatient(
                                PATIENT_WITH_A_SINGLE_SWITCH_ID,
                                patient ->
                                        patient.withPurchase(MedicationType.B)
                                                .withPurchase(MedicationType.I)
                                                .withTrial(MedicationType.I)
                                                .withPurchases(MedicationType.B, 5))
                        .withPatient(
                                PATIENT_WITH_A_MULTIPLE_SWITCHES_ID,
                                patient ->
                                        patient.withPurchases(MedicationType.I, 2)
                                                .withPurchase(MedicationType.B)
                                                .withPurchases(MedicationType.I, 3)
                                                .withPurchase(MedicationType.B))
                        .compile());

        AnalysisResult result = biAnalysisService.performBIAnalysis();

        assertThat("Valid I to B switches should be 1.", result.getTotal(VALID_IB_SWITCH), is(1));
        assertThat("Valid B to I switches should be 1.", result.getTotal(VALID_BI_SWITCH), is(1));
    }

    @Test
    public void shouldCorrectlyReportValidNoComed() throws Exception {
        setupPatients(
                recordsBuilder()
                        .withPatient(1, (builder) -> builder.withPurchases(MedicationType.B, 10))
                        .withPatient(2, (builder) -> builder.withPurchases(MedicationType.I, 10))
                        .compile());

        AnalysisResult result = biAnalysisService.performBIAnalysis();

        assertThat("Valid no co-med should be 2.", result.getTotal(VALID_NO_COMED), is(2));
    }

    private RecordsBuilder recordsBuilder() {
        return new RecordsBuilder();
    }

    private void setupPatients(List<PurchaseRecord> values) throws Exception {
        when(purchaseRecordDAO.allPurchaseRecords()).thenReturn(values);
    }

    private static class PatientBuilder {
        private final Integer patientId;
        private Optional<Integer> currentGap = Optional.empty();
        private Optional<PurchaseRecord> lastPurchase = Optional.empty();
        private List<PurchaseRecord> purchases = new ArrayList<>();

        public PatientBuilder(Integer id) {
            this.patientId = id;
        }

        public PatientBuilder withGap(int gap) {
            this.currentGap = Optional.of(gap);
            return this;
        }

        public PatientBuilder withPurchases(MedicationType type, int number) {
            for (int i = 0; i < number; i++) {
                withPurchase(type);
            }

            return this;
        }

        public PatientBuilder withTrial(MedicationType type) {
            Optional<Integer> trialStartDate =
                    this.lastPurchase.map(
                            v ->
                                    v.getDay()
                                            + MedicationType.valueOf(v.getMedication()).duration()
                                                    / 2);
            PurchaseRecord record =
                    new PurchaseRecord(trialStartDate.orElse(0), type.name(), this.patientId);
            this.lastPurchase = Optional.of(record);
            this.purchases.add(record);
            return this;
        }

        public PatientBuilder withPurchase(MedicationType type) {
            Optional<Integer> previousPurchaseDate =
                    this.lastPurchase.map(
                            v -> v.getDay() + MedicationType.valueOf(v.getMedication()).duration());
            PurchaseRecord record =
                    new PurchaseRecord(
                            previousPurchaseDate.map(v -> v + 1).orElse(0)
                                    + this.currentGap.orElse(0),
                            type.name(),
                            this.patientId);
            this.currentGap = Optional.empty();
            this.lastPurchase = Optional.of(record);
            this.purchases.add(record);
            return this;
        }
    }

    private static class RecordsBuilder {
        private final List<PatientBuilder> patients = new ArrayList<>();

        public RecordsBuilder withPatient(Integer id, Consumer<PatientBuilder> handler) {
            PatientBuilder builder = new PatientBuilder(id);
            this.patients.add(builder);
            handler.accept(builder);
            return this;
        }

        public List<PurchaseRecord> compile() {
            return patients.stream()
                    .flatMap(v -> v.purchases.stream())
                    .collect(Collectors.toList());
        }
    }
}
