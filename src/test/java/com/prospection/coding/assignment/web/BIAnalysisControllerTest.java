package com.prospection.coding.assignment.web;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.prospection.coding.assignment.data.PurchaseRecordCsvDAO;
import com.prospection.coding.assignment.domain.AnalysisResult;
import com.prospection.coding.assignment.domain.MedicationType;
import com.prospection.coding.assignment.domain.PatientData;
import com.prospection.coding.assignment.service.BIAnalysisService;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@ContextConfiguration(
        classes = {BIAnalysisController.class, PurchaseRecordCsvDAO.class, BIAnalysisService.class})
@WebMvcTest
public class BIAnalysisControllerTest {
    @Autowired private MockMvc mockMvc;

    @Autowired private BIAnalysisService service;

    @Test
    public void testBIAnalysisEndpoint() throws Exception {
        MvcResult result =
                mockMvc.perform(
                                MockMvcRequestBuilders.get("/bi/analysis")
                                        .accept(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk())
                        .andReturn();

        AnalysisResult expectedResult = service.performBIAnalysis();

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonObject = mapper.readTree(result.getResponse().getContentAsString());
        assertTrue("response JSON is missing 'patients' node", jsonObject.has("patients"));
        assertTrue(
                "response JSON is missing 'patientTypeNameMap' node",
                jsonObject.has("patientTypeNameMap"));

        JsonNode patientsJson = jsonObject.get("patients");
        JsonNode patientTypeNameMapJson = jsonObject.get("patientTypeNameMap");

        for (AnalysisResult.PatientType type : AnalysisResult.PatientType.values()) {
            int returnedValue = patientsJson.get(type.name()).intValue();
            assertEquals(
                    "Incorrect value returned for " + type.name(),
                    expectedResult.getTotal(type),
                    Integer.valueOf(returnedValue));

            String typeName = patientTypeNameMapJson.get(type.name()).asText();
            assertEquals(
                    "Incorrect type name returned for " + type.name(),
                    expectedResult.getPatientTypeNameMap().get(type),
                    typeName);
        }
    }

    @Test
    public void testMedicationsEndpoint() throws Exception {
        MvcResult result =
                mockMvc.perform(
                                MockMvcRequestBuilders.get("/bi/medications")
                                        .accept(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk())
                        .andReturn();

        MedicationType[] expectedTypes = MedicationType.values();

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonObject = mapper.readTree(result.getResponse().getContentAsString());
        assertTrue("JSON object expected", jsonObject.isObject());

        for (MedicationType type : expectedTypes) {
            assertTrue(
                    "Expected medication type is not present in the response JSON",
                    jsonObject.has(type.name()));

            int returnedValue = jsonObject.get(type.name()).intValue();
            assertEquals(
                    "Incorrect duration value for " + type.name(),
                    Integer.valueOf(returnedValue),
                    Integer.valueOf(type.duration()));
        }
    }

    @Test
    public void testBIPatientsEndpoint() throws Exception {
        for (AnalysisResult.PatientType type : AnalysisResult.PatientType.values()) {
            MvcResult result =
                    mockMvc.perform(
                                    MockMvcRequestBuilders.get("/bi/patients/" + type.name())
                                            .accept(MediaType.APPLICATION_JSON))
                            .andExpect(status().isOk())
                            .andReturn();

            List<PatientData> expectedRecords =
                    service.processPatientData(new BIAnalysisService.PatientDataTypePredicate(type))
                            .collect(Collectors.toList());

            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            mapper.setVisibility(PropertyAccessor.CREATOR, JsonAutoDetect.Visibility.ANY);

            JsonNode jsonObject = mapper.readTree(result.getResponse().getContentAsString());
            assertTrue("JSON object expected", jsonObject.isArray());
            assertEquals("Records count mismatch", expectedRecords.size(), jsonObject.size());

            for (int i = 0; i < expectedRecords.size(); i++) {
                JsonNode node = jsonObject.get(i);
                PatientData data = mapper.treeToValue(node, PatientData.class);
                assertEquals(expectedRecords.get(i), data);
            }
        }
    }
}
